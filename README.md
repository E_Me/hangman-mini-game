Hangman Game

This is the classic Hangman game where the player tries to guess the hidden word by adding one character at a time. There are 8 attempts in total to guess the whole word.

Screenshot from the game:

![](Images/Hangman1.png)
